const Game = require('./game.js');
const Human = require('./human.js');
const DumbAI = require('./dumb-ai.js');

// multiplayer
const human1 = new Human('Human 1', 'x');
const human2 = new Human('Human 2', 'o');

const game = new Game(3, human1, human2);

/*
// play against the dumbest AI ever!
const human1 = new Human('Human 1', 'x');
const dumbAI = new DumbAI('o');

const game = new Game(3, human1, dumbAI);
*/

game.run().then(winner => {
    console.log(game.showBoard());

    if (!winner) {
        console.log('Tie!');
        return;
    }

    console.log(`${winner.name} wins!`);
});
