const matMul = require('./util').matrixMultiplication;
const logisticRegression = require('./util').logisticRegression;
const util = require('./util.js');


module.exports = class NeuralNetwork {
  //as of right now, this NN is limited to
  // 2 hidden layers with 10 neurons each
  constructor(){
    this.inputWeightMatrix = util.range(10).map(
        _ => util.range(9).map(_ => Math.floor(Math.random() * 10) - 5)
    );
    this.hiddenWeightMatrix = util.range(10).map(
        _ => util.range(10).map(_ => Math.floor(Math.random() * 10) - 5)
    );
    this.outputWeightMatrix = util.range(9).map(
        _ => util.range(10).map(_ => Math.floor(Math.random() * 10) - 5)
    );
  }

  //the paramater is a array of length 9
  //with 0: NO_PLAYER; 1: OPPONENT; 2:THIS
  getMove(boardArray){
    const activation1 = logisticRegression(matMul(this.inputWeightMatrix, boardArray));
    const activation2 = logisticRegression(matMul(this.hiddenWeightMatrix, activation1));
    const output = logisticRegression(matMul(this.outputWeightMatrix, activation2));
    return output;
  }

  test(){
    const brain1 = new NN();
    const boardArray = [0, 0, 1, 0, 2, 0, 0, 0, 0];
    console.log(brain1.getMove(boardArray));
  }
}
