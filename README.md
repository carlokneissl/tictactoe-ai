# tictactoe-ai

A TicTacToe game against a dumb AI that is supposed to develop into a genetic
algorithm in the future.

## Get it

Run:

```
git clone https://gitlab.com/petermader/tictactoe-ai
```

## Run it

In the directory you installed `tictactoe-ai` in, run:

```
node .
```
